Docker for Laravel 
---
#### Containers:
- Nginx
- PHP
- Mysql
- PhpMyAdmin
- Mongo
#### Config .env:
DB_HOST=name_mysql_container


Up containers
```
$ docker-compose up -d
```

Show all started contaners 
you can get name mysql container for DB_HOST and name php container for docker exec
```
$ docker ps 
```

Enter the container for migrations
```
$ docker exec -it name_php_container bash 
```

Stop containers 
```
$ docker-compose down
```

Import mysqldump
```
$ docker-compose exec mysql bash
$ cd /var/lib/mysql
$ mysql -u root -p db_name < dump_name
```
